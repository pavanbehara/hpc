 
#include<stdio.h> 
#include<math.h> 
#include<stdlib.h> 
#include<time.h>
#include<omp.h> 
 
void main() 
{
double evalfunc(double h,double l,double b,double t); 

double start=omp_get_wtime( );
 
int D=4;					/*D = no. of varibales on which constriants are placed, x ={h, l, t, b}*/ 
double x_lower[4] = {0.125, 0.1, 0.1, 0.1};	/*lower limits of the varibales x ={h, l, t, b} */ 
double x_upper[4] = {10, 10, 10, 10};		/*upper limits of the varibales x ={h, l, t, b} */ 
double CR=0.8;					/*Crossover constant*/ 
double F=0.4;					/*Scaling factor*/ 
int Np=200;					/*Population size*/ 
double pop1[Np][D], f1[Np], *Xt, *Xa, *Xb, *Xc, Xtr[D], f1Xtr, f1Xt,Xc1[D], taud,taudd, tau, sigma, pc, delta, Diff[D], Wdiff[D]; 
int flag, i, j, gen, k, c, r1, r2, r3, n[D],temp=0;  
srand(time(NULL));

for (i=0; i<Np; i++) 
{ 
    for (j=0; j<D; j++) 
    { 
    pop1[i][j]=x_lower[j]+(x_upper[j]-x_lower[j])*((rand()%32767+1)/32767.0f);      /*Generating population*/
	} 
} 
	 
for (i=0; i<Np; i++) 
{ 
    f1[i]=evalfunc(pop1[i][0],pop1[i][1],pop1[i][2],pop1[i][3]);                   /*Evaluating function value*/
} 
    
  
   
for (gen=1; gen<=1000; gen++) 
{	for (i=0; i<Np; i++)
{	Xt=(pop1[i]);  
	for(int kt=0; kt<D; kt++)
	{n[kt] = floor(((rand()%32767+1)/32767.0f)*D);} 
	flag=0; 
    for(k=0; k<D; k++)
{ 
        c=((rand()%32767+1)/32767.0f);
 
    if ((c<CR)||(k==D))
{ 	
    if(flag==0)
{ 
        flag=1; 
        r1=floor(((rand()%32767+1)/32767.0f)*Np); 
        while(r1==i) 
        {r1=floor(((rand()%32767+1)/32767.0f)*Np);} 
        r2=floor(((rand()%32767+1)/32767.0f)*Np); 
        while(r2==i||r2==r1) 
        {r2=floor(((rand()%32767+1)/32767.0f)*Np);} 
        r3=floor(((rand()%32767+1)/32767.0f)*Np); 
        while(r3==i||r3==r1||r3==r2) 
        {r3=floor(((rand()%32767+1)/32767.0f)*Np);} 
        
        Xa=pop1[r1]; 
        Xb=pop1[r2]; 
        Xc=pop1[r3]; 
    for(int z=0;z<D;z++)                        
	Diff[z]=*(Xa+z)-*(Xb+z); 
	F=((rand()%32767+1)/32767.0f);  /*scaling factor*/  
    for(int jt=0;jt<D;jt++)
	Wdiff[jt]=F*Diff[jt]; 
    for(int g=0;g<D;g++)
    	Xc1[g]=*(Xc+g)+Wdiff[g]; 
} 
temp=n[k];    
Xtr[temp]=Xc1[temp]; 
}
    else 
    Xtr[temp]=*(Xt+temp);   /*Trial vector*/
}   
}
           
for (j=0;j<D;j++)  /*if outside the bounds  then map*/ 
   if((Xtr[j]<x_lower[j]) || (Xtr[j]>x_upper[j])) 
     Xtr[j]=x_lower[j] + (x_upper[j]-x_lower[j])*((rand()%32767+1)/32767.0f); 
             
             
	f1Xt=f1[i]; 
    
 /*Constraint handling by penalty method*/
 taud = 6000/(sqrt(2)*(Xtr[0])*(Xtr[1]));            /* first differential of tau*/
 taudd = 6000*(14+0.5*(Xtr[1]))*sqrt(0.25*(pow(Xtr[1],2.)+pow(Xtr[0]+Xtr[2],2.)))/(2*(0.707*(Xtr[0])*(Xtr[1])*((pow(Xtr[1],2.)/12)+0.25*pow(Xtr[0]+Xtr[2],2.)))); /* second differential of tau*/
 tau = sqrt(pow(taud,2.)+pow(taudd,2.)+((Xtr[1]*taud*taudd)/sqrt(0.25*(pow(Xtr[1],2.)+pow(Xtr[0]+Xtr[2],2.)))));
sigma = 504000/(pow(Xtr[2],2.)*(Xtr[3]));
pc = 64746.022*(1-0.0282346*(Xtr[2]))*(Xtr[2])*pow(Xtr[3],3.);
delta = 2.1952/(pow(Xtr[2],3.)*(Xtr[3]));

if((13600-tau)>=0 && (30000-sigma)>=0 && (Xtr[2]-Xtr[0])>=0 && (pc-6000)>=0 && (0.25-sigma)>=0)
f1Xtr=evalfunc(Xtr[0],Xtr[1],Xtr[2],Xtr[3]); 
else
f1Xtr = 1E10;

	if (f1Xt>f1Xtr) 
	{for(int jh=0;jh<D;jh++)
	 pop1[i][jh]=Xtr[jh]; 
         f1[i]=f1Xtr;};
}             
                     

for (int ii=1; ii<Np ;ii++) 
printf("%g \t %g \t %g \t %g \t %g \n",f1[ii],pop1[ii][0],pop1[ii][1],pop1[ii][2],pop1[ii][3]); 
    
double end=omp_get_wtime( ); 
double cpu_time_used = end-start;
printf("%g \n",cpu_time_used); 
}
 
double evalfunc(double h,double l,double b,double t) 
{ 
double funcvalue;
funcvalue = 1.10471*(pow(h,2.))*l+0.04811*t*b*(14+l); 
return(funcvalue); 
} 
 
 
 
