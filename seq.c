#include <omp.h>
#include <stdio.h>
#include <math.h>
#include <time.h>

#define N 10000 
#define ITER 200 // total no of iterations 
#define MASTER 0 // id of the first processor

#define BEGIN 1 // message type
#define DONE 2 // message type
#define BOTTOM 3 // message type
#define TOP 4 // message type

main(int argc,char *argv[])
{

int Nx = N;
int Ny = N;



float u[2][Ny][Nx]; //variable to solve
int min_rows, overflow;
int slave, bottom, top;
int num_rows; // num_rows for each processor
int destination, source; // for convenient msg passing
int msg;
int base;
int i,j,k;
int start, end; //row wise domain distribution for each processor 
float eps=0.1;
int count;
double start_time,end_time;
start_time = omp_get_wtime();  /*Master Task. Dividing the data among processors and collects data back. No computation performed by master*/
for (i=0; i<Nx; i++)
{
u[1][0][i]=u[0][0][i];
for (i=0; i<Nx; i++)
u[1][Ny-1][i]=u[0][Ny-1][i];
for (j=0; j<Ny; j++)
u[1][j][0]=u[0][j][0];
for (j=0; j<Ny; j++)
u[1][j][Nx-1]=u[0][j][Nx-1];
if (base==0)
start=1; 
else
start=base;
if (base+num_rows==Ny)
end= base + num_rows-2; 
else
end = base + num_rows-1;
k=0;
}
for (j = start; j <= end; j++)
for (i = 1; i <= Nx-2; i++)
{u[1-k][j][i] = (u[k][j][i+1]+u[k][j][i-1]+u[k][j+1][i]+u[k][j-1][i])*0.25;
k = 1 - k;
}
end_time=omp_get_wtime();
printf ( " Total Elapsed time for pure MPI implementation is %f seconds\n", end_time-start_time);
}
/*Subroutines*/
void init(int nx, int ny, float *u)
{
int i,j;
double hx, hy;
hx=1.0/(nx-1);
hy=1.0/(ny-1);
for (j = 1; j <= ny-2; j++)
for (i = 1; i <= nx-2; i++)
*(u+j*nx+i) = (float)(0); //all the interior points are set to 0
j=0;
for (i = 0; i <= nx-1; i++)
*(u+j*nx+i) = sin(3.14*i*hx); 
j=ny-1;
for (i = 0; i <= nx-1; i++)
*(u+j*nx+i) = exp(-3.14)*sin(3.14*i*hx); 
i=0;
for (j = 0; j <= ny-1; j++)
*(u+j*nx+i) = (float)(0); 
i=nx-1;
for (j = 0; j <= ny-1; j++)
*(u+j*nx+i) = (float)(0); 
}
void save(int nx, int ny, float *u1, char *output)
{
int i, j;
FILE *f_out;
f_out = fopen(output, "w");
for (i = nx-1; i >=0; i--)
for (j = 0; j <= ny-1; j++)
fprintf(f_out, "%d %d %6.12f\n", i, j, *(u1+j*nx+i));
fclose(f_out);
}
float errorcheck(int start, int end, int nx, int ny, float *u)
{
int i,j;
float sum = 0.0;
float exact;
double hx, hy;
hx=1.0/(nx-1);
hy=1.0/(ny-1);
for (j = start; j <= end; j++)
{
for (i = 1; i <= nx-2; i++)
{
exact = (sin(3.14*i*hx))*exp(-3.14*j*hy); 
sum = sum + (exact - *(u+j*nx+i))*(exact - *(u+j*nx+i));
}
}
return sqrt(sum);
}

/*END OF PROGRAM*/
